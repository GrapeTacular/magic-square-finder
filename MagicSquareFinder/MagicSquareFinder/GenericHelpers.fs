﻿module GenericHelpers

let cartProduct<'t, 'u> (x : List<'t>) (y : List<'u>) : List<'t * 'u> =
    let getLine (t : 't) : List<'t * 'u> = [ for u in y -> (t, u) ]
    let getLines () : List<List<'t * 'u>> = [ for t in x -> getLine t ]
    List.concat (getLines ())

let rec allElementsEqual<'t when 't : equality> (elements : List<'t>) : bool =
    match elements with
    | h :: i :: remaining ->
        if h = i then
            allElementsEqual (i :: remaining)
        else
            false
    | _ ->
        true

﻿module MagicSquareTests

open GenericHelpers
open MagicSquareHelpers


// helpers

let getUniqueIntSquare1to9 () : Map<int * int, int> =
    let oneToThree = [ 1 .. 3 ]
    let product = cartProduct oneToThree oneToThree
    Map.ofList [ for (r, c) in product -> ((r, c), (r - 1) * 3 + c) ]

let getSquareOf5 () : Map<int * int, int> =
    let oneToThree = [ 1 .. 3 ]
    let product = cartProduct oneToThree oneToThree
    Map.ofList [ for (r, c) in product -> ((r, c), 5) ]

let equalSquares (left : Map<int * int, int>) (right : Map<int * int, int>) : bool =
    let leftItems = Map.toList left
    let rightItems = Map.toList right
    leftItems = rightItems

let printSquare (square : Map<int * int, int>) : unit =
    let printer (s : ((int * int) * int)) : unit =
        match s with
        | ((_, c), v) ->
            if c = 3 then
                printfn "%i\t" v
            else 
                printf "%i\t" v
        | _ ->
            ()

    List.iter printer (Map.toList square)
    printf "\n"


// main

let runCompoundHelpersTest () : unit =
    let sentinel = 0
    let replacementIndex = (2, 2)
    let uniqueInts = getUniqueIntSquare1to9 ()
    let uniqueIntsWithsentinel = Map.add replacementIndex sentinel (Map.remove replacementIndex uniqueInts)
    let nonUniqueInts = Map.add replacementIndex 1 (Map.remove replacementIndex uniqueInts)
    let fives = getSquareOf5 ()
    let fivesWithsentinel = Map.add replacementIndex sentinel (Map.remove replacementIndex fives)
    let notAllfives = Map.add replacementIndex 4 (Map.remove replacementIndex fives)

    printSquare uniqueInts
    printfn "all elements unique: %b\n" (allElementsUnique uniqueInts 3)
    printSquare uniqueIntsWithsentinel
    printfn "all non-sentinel elements unique: %b\n" (allNonSentinelElementsUnique uniqueIntsWithsentinel 3 sentinel)
    printSquare nonUniqueInts
    printfn "all non-sentinel elements unique: %b\n" (allNonSentinelElementsUnique nonUniqueInts 3 sentinel)
    printSquare fives
    printfn "all equal sums: %b\n" (allEqualSums fives 3)
    printSquare fivesWithsentinel
    printfn "all equal non-sentinel sums: %b\n" (allEqualNonSentinelSums fivesWithsentinel 3 sentinel)
    printSquare notAllfives
    printfn "all equal non-sentinel sums: %b\n" (allEqualNonSentinelSums notAllfives 3 sentinel)

let runCloseToMagicSquareTest (dimension : int) (sentinel : int) (limit : int) : unit =
    let closeToMagicSquare = Map.ofList [
        ((1, 1), 2);
        ((1, 2), 7);
        ((1, 3), 6);
        ((2, 1), 9);
        ((2, 2), 5);
        ((2, 3), 1);
        ((3, 1), 4);
        ((3, 2), 3);
        ((3, 3), 0);
    ]

    let replacementIndex = (3, 3)
    let magicSquare = Map.add replacementIndex 8 (Map.remove replacementIndex closeToMagicSquare)
    
    match (getMagicSquares dimension sentinel limit closeToMagicSquare) with
    | head :: tail ->
        printSquare head
        printSquare magicSquare
        printfn "found a magic square: %b" (head = magicSquare)
    | _ ->
        printfn "did not found a magic square"


let runCompleteTest (dimension : int) (sentinel : int) (limit : int) : unit =
    let squareTest = getSquareOfSentinels dimension sentinel
    let magicSquares = getMagicSquares dimension sentinel limit squareTest
    printf "\n"
    List.iter printSquare magicSquares
    printfn "all magic squares of dimension %i and under limit of %i" dimension limit

﻿open System

[<EntryPoint>]
let main argv =
    try
        let input = argv.[0]

        if input = "test" then
            printf "\n"
            MagicSquareTests.runCompoundHelpersTest ()
            MagicSquareTests.runCloseToMagicSquareTest 3 0 1
            MagicSquareTests.runCompleteTest 3 0 10
        else
            let dimension = argv.[0] |> int
            let limit = argv.[1] |> int
            let sentinel = 0

            let seedSquare = MagicSquareHelpers.getSquareOfSentinels dimension sentinel
            let magicSquares = MagicSquareHelpers.getMagicSquares dimension sentinel limit seedSquare
            List.iter MagicSquareTests.printSquare magicSquares
    with
    | :? Exception as ex ->
        printfn "%s" (ex.ToString ())

    0 // return an integer exit code
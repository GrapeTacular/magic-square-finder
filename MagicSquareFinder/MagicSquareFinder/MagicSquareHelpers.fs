﻿module MagicSquareHelpers

open GenericHelpers


// helpers

let getIndices (dimension : int) : List<(int * int)> =
    let oneToDim = [ 1 .. dimension ]
    cartProduct oneToDim oneToDim

let getElements (square : Map<int * int, int>) (dimension : int) : List<int> =
    let indices = getIndices dimension
    [ for i in indices -> square.Item i ]

let getNonSentinelElements (square : Map<int * int, int>) (dimension : int) (sentinel : int) : List<int> =
    let elements = getElements square dimension
    List.filter (fun v -> v <> sentinel) elements

let allElementsUnique (square : Map<int * int, int>) (dimension : int) : bool =
    let elements = getElements square dimension
    let elementSet = Set.ofList elements
    List.length elements = Set.count elementSet

let allNonSentinelElementsUnique (square : Map<int * int, int>) (dimension : int) (sentinel : int) : bool =
    let nonSentinelElements = getNonSentinelElements square dimension sentinel
    let nonSentinelSet = Set.ofList nonSentinelElements
    List.length nonSentinelElements = Set.count nonSentinelSet

let getRowsOfElements (square : Map<int * int, int>) (dimension : int) : List<List<int>> =
    let getRow (r : int) : List<int> = [ for c in 1 .. dimension -> square.Item (r, c) ]
    [ for r in 1 .. dimension -> getRow r ]

let getColsOfElements (square : Map<int * int, int>) (dimension : int) : List<List<int>> =
    let getCol (c : int) : List<int> = [ for r in 1 .. dimension -> square.Item (r, c) ]
    [ for c in 1 .. dimension -> getCol c ]

let getDiagonalsOfElements (square : Map<int * int, int>) (dimension : int) : List<List<int>> =
    let dia1 = [ for d in 1 .. dimension -> square.Item (d, d) ]
    let dia2 = [ for d in 1 .. dimension -> square.Item (d, (dimension - d + 1)) ]
    [ dia1; dia2; ]

let getSums (elementLists : List<List<int>>) : List<int> =
    let getSum (elements : List<int>) : int = List.fold (+) 0 elements
    List.map getSum elementLists

let getNonSentinelElementLists (elementLists : List<List<int>>) (sentinel : int) : List<List<int>> =
    let isWithoutSentinel (elements : List<int>) : bool = not (List.contains sentinel elements)
    List.filter isWithoutSentinel elementLists

let allEqualSums (square : Map<int * int, int>) (dimension : int) : bool =
    let rowSums = getSums (getRowsOfElements square dimension)
    let colSums = getSums (getColsOfElements square dimension)
    let diaSums = getSums (getDiagonalsOfElements square dimension)
    allElementsEqual (diaSums @ rowSums @ colSums)

let allEqualNonSentinelSums (square : Map<int * int, int>) (dimension : int) (sentinel : int) : bool =
    let nonsentinelRows = getNonSentinelElementLists (getRowsOfElements square dimension) sentinel
    let nonsentinelCols = getNonSentinelElementLists (getColsOfElements square dimension) sentinel
    let nonsentinelDiagonals = getNonSentinelElementLists (getDiagonalsOfElements square dimension) sentinel
    let rowSums = getSums nonsentinelRows
    let colSums = getSums nonsentinelCols
    let diaSums = getSums nonsentinelDiagonals
    allElementsEqual (diaSums @ rowSums @ colSums)


// compound helpers

let getSquareOfSentinels (dimension : int) (sentinel : int) : Map<int * int, int> =
    let seed = Map.empty
    let indices = getIndices dimension
    List.fold (fun square index -> Map.add index sentinel square) seed indices

let getLocationsOfSentinels (square : Map<int * int, int>) (sentinel : int) : List<int * int> =
    let folder (locations : List<int * int>) (key : int * int) (v : int) : List<int * int> =
        if v = sentinel then
            locations @ [ key; ]
        else
            locations

    Map.fold folder List<int * int>.Empty square

let getUnusedElements (square : Map<int * int, int>) (dimension : int) (sentinel : int) : List<int> =
    let usedElements = getElements square dimension
    List.except usedElements [ 1 .. dimension * dimension ]

let isPotentialMagicSquare (square : Map<int * int, int>) (dimension : int) (sentinel : int) : bool =
    (allNonSentinelElementsUnique square dimension sentinel) &&
    (allEqualNonSentinelSums square dimension sentinel)

let isMagicSquare (square : Map<int * int, int>) (dimension : int) : bool =
    (allElementsUnique square dimension) &&
    (allEqualSums square dimension)


// main

let getMagicSquares
    (dimension : int)
    (sentinel : int)
    (limit : int)
    (seedSquare : Map<int * int, int>)
    : List<Map<int * int, int>> =

    let mutable magicSquares = List<Map<int * int, int>>.Empty

    let rec backTrack (square : Map<int * int, int>) : unit =
        if (List.length magicSquares >= limit) then
            ()
        else if (isMagicSquare square dimension) then
            magicSquares <- magicSquares @ [ square; ]
        else
            let unusedElements = getUnusedElements square dimension sentinel
            let locationsOfsentinels = getLocationsOfSentinels square sentinel

            let makeIterator (loc : (int * int)) (v : int) : unit =
                let nextSquare = Map.add loc v (Map.remove loc square)
                if (isPotentialMagicSquare nextSquare dimension sentinel) then
                    backTrack nextSquare
                else
                    ()

            if (List.length locationsOfsentinels) > 0 then
                let loc = List.head locationsOfsentinels
                List.iter (makeIterator loc) unusedElements
            else
                ()

    backTrack seedSquare

    magicSquares
